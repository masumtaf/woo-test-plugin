<?php 
/**
 * Plugin Name: Woo Test Plugin
 * Plugin URI: https://abdullah.com/
 * Description: Woo Test Plugin — The Best Gutenberg Blocks Collection for WooCommerce
 * Author: abdullah.com
 * Author URI: https://abdullah-port.com/
 * Version: 1.0.0
 * License: GPL2+
 * License URI: https://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain: woo-test-plugin
 *
 * @package Test Plugin
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

require __DIR__ . '/vendor/autoload.php';

/**
 * Initialize the plugin tracker
 *
 * @return void
 */
function appsero_init_tracker_wwoo_test_plugin() {

    if ( ! class_exists( 'Appsero\Client' ) ) {
      require_once __DIR__ . '/appsero/src/Client.php';
    }

    $client = new Appsero\Client( 'e3d9c352-2654-426c-b627-9e15a6ff2d38', 'wWoo Test Plugin', __FILE__ );

    // Active insights
    $client->insights()->init();

    // Active automatic updater
    $client->updater();

}

appsero_init_tracker_wwoo_test_plugin();